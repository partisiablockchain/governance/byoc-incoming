package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A dispute for a BYOC deposit, which is used to settle disagreements over performed deposits.
 * Disputes are settled by an automated vote performed by the large oracle, ie. the entire block
 * producer committee.
 */
@Immutable
public final class Dispute implements StateSerializable {

  /** The node operator that initiated the dispute. */
  private final BlockchainAddress challenger;

  /**
   * The claimed alternative deposits that is part of the dispute. All claims differ from the
   * original deposit in the state.
   */
  private final FixedList<Transaction> claims;

  /**
   * The index of the claimed deposit that won the vote. The index -1 specifies that the original
   * deposit was the winner. Null until a winner is found.
   */
  private final Integer votingResult;

  @SuppressWarnings("unused")
  Dispute() {
    challenger = null;
    claims = null;
    votingResult = null;
  }

  private Dispute(
      BlockchainAddress challenger, FixedList<Transaction> claims, Integer votingResult) {
    this.challenger = challenger;
    this.claims = claims;
    this.votingResult = votingResult;
  }

  /**
   * Create dispute state.
   *
   * @param challenger initiator of dispute
   * @param transaction object of dispute
   * @return new dispute state
   */
  public static Dispute create(BlockchainAddress challenger, Transaction transaction) {
    return new Dispute(challenger, FixedList.create(List.of(transaction)), null);
  }

  static Dispute fromStateAccessor(StateAccessor accessor) {
    BlockchainAddress challenger = accessor.get("challenger").blockchainAddressValue();
    List<Transaction> claims =
        accessor.get("claims").getListElements().stream()
            .map(Transaction::fromStateAccessor)
            .toList();
    Integer votingResult;
    StateAccessor result = accessor.get("votingResult");
    if (result.isNull()) {
      votingResult = null;
    } else {
      votingResult = result.intValue();
    }
    return new Dispute(challenger, FixedList.create(claims), votingResult);
  }

  /**
   * Get challenger of dispute.
   *
   * @return address of challenger
   */
  public BlockchainAddress getChallenger() {
    return challenger;
  }

  /**
   * Get all claims.
   *
   * @return all claims
   */
  public List<Transaction> getClaims() {
    return new ArrayList<>(Objects.requireNonNull(claims));
  }

  /**
   * Get voting result.
   *
   * @return voting result
   */
  public Integer getVotingResult() {
    return votingResult;
  }

  /**
   * Add voting result.
   *
   * @param votingResult to add
   * @return next dispute state
   */
  public Dispute addVotingResult(int votingResult) {
    return new Dispute(challenger, claims, votingResult);
  }

  /**
   * Get the original claim.
   *
   * @return original claim
   */
  public Transaction getOriginalClaim() {
    return Objects.requireNonNull(claims).get(0);
  }

  /**
   * Get the claim that has won the vote.
   *
   * @return the winning claim, or null if the vote is not done
   */
  public Transaction getWinningClaim() {
    if (votingResult == null) {
      return null;
    } else {
      return Objects.requireNonNull(claims).get(votingResult);
    }
  }

  /**
   * Add counter-claim to the dispute, if it does not already exist.
   *
   * @param transaction the countering external transaction
   * @return a new dispute with the new counter-claim, or the same dispute if the claim is already
   *     made
   */
  public Dispute addCounterClaim(Transaction transaction) {
    if (Objects.requireNonNull(claims).contains(transaction)) {
      return this;
    } else {
      return new Dispute(
          challenger, Objects.requireNonNull(claims).addElement(transaction), votingResult);
    }
  }

  /**
   * A claimed deposit that is part of a dispute.
   *
   * @param nonce The deposit nonce of the claimed deposit
   * @param receiver The receiver of the claimed deposit
   * @param amount The amount of the claimed deposit
   */
  @Immutable
  public record Transaction(long nonce, BlockchainAddress receiver, Unsigned256 amount)
      implements StateSerializable {

    // PMD bug reports a false positive of UnusedPrivateMethod when inner class function is passed
    // as reference. See https://github.com/pmd/pmd/issues/3292
    static Transaction fromStateAccessor(StateAccessor accessor) {
      long nonce = accessor.get("nonce").longValue();
      BlockchainAddress receiver = accessor.get("receiver").blockchainAddressValue();
      Unsigned256 amount = accessor.get("amount").cast(Unsigned256.class);
      return new Transaction(nonce, receiver, amount);
    }
  }
}
