package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;

/** A historic BYOC deposit oracle that handled a number of deposits. */
@Immutable
public final class Epoch implements StateSerializable {

  /** The first deposit nonce that the historic oracle handled (inclusive). */
  private final long fromDepositNonce;

  /** The last deposit nonce that the historic oracle handled (exclusive). */
  private final long toDepositNonce;

  /** The nodes that served as BYOC deposit oracle during the epoch. */
  private final FixedList<BlockchainAddress> oracles;

  @SuppressWarnings("unused")
  Epoch() {
    fromDepositNonce = 0L;
    toDepositNonce = 0L;
    oracles = null;
  }

  Epoch(long fromDepositNonce, long toDepositNonce, FixedList<BlockchainAddress> oracles) {
    this.fromDepositNonce = fromDepositNonce;
    this.toDepositNonce = toDepositNonce;
    this.oracles = oracles;
  }

  static Epoch fromStateAccessor(StateAccessor accessor) {
    long fromDepositNonce = accessor.get("fromDepositNonce").longValue();
    long toDepositNonce = accessor.get("toDepositNonce").longValue();
    List<BlockchainAddress> oracles =
        accessor.get("oracles").getListElements().stream()
            .map(StateAccessor::blockchainAddressValue)
            .toList();
    return new Epoch(fromDepositNonce, toDepositNonce, FixedList.create(oracles));
  }

  /**
   * Get the first deposit nonce that the historic oracle handled (inclusive).
   *
   * @return first nonce in this epoch
   */
  public long getFromDepositNonce() {
    return fromDepositNonce;
  }

  /**
   * Get the last deposit nonce that the historic oracle handled (exclusive).
   *
   * @return last nonce in this epoch
   */
  public long getToDepositNonce() {
    return toDepositNonce;
  }

  /**
   * Get the nodes that served as BYOC deposit oracle during the epoch.
   *
   * @return oracle addresses
   */
  public List<BlockchainAddress> getOracles() {
    return new ArrayList<>(oracles);
  }
}
