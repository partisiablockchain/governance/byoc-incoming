package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Objects;

/**
 * A deposit is a transfer of a coin from the external blockchain to PBC. On PBC the deposited coin
 * is minted as a twin BYOC coin.
 */
@Immutable
public final class Deposit implements StateSerializable {

  /** The receiver of the deposit that will receive the minted BYOC coins on PBC. */
  private final BlockchainAddress receiver;

  /** The amount deposited. Note that the integer amount includes the decimals of the coin. */
  private final Unsigned256 amount;

  /** A bitmap describing which oracle nodes have signed the deposit. */
  private final int signaturesRegistered;

  @SuppressWarnings("unused")
  Deposit() {
    receiver = null;
    amount = null;
    signaturesRegistered = 0;
  }

  /**
   * Default constructor.
   *
   * @param receiver address of deposit receiver
   * @param amount amount to deposit
   */
  public Deposit(BlockchainAddress receiver, Unsigned256 amount) {
    this(receiver, amount, 0);
  }

  private Deposit(BlockchainAddress receiver, Unsigned256 amount, int signaturesRegistered) {
    this.receiver = receiver;
    this.amount = amount;
    this.signaturesRegistered = signaturesRegistered;
  }

  static Deposit fromStateAccessor(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      BlockchainAddress receiver = accessor.get("receiver").blockchainAddressValue();
      Unsigned256 amount = accessor.get("amount").cast(Unsigned256.class);
      int signaturesRegistered = accessor.get("signaturesRegistered").intValue();
      return new Deposit(receiver, amount, signaturesRegistered);
    }
  }

  /**
   * Get the receiver of the deposit that will receive the minted BYOC coins on PBC.
   *
   * @return The address of the receiver
   */
  public BlockchainAddress receiver() {
    return receiver;
  }

  /**
   * Get the amount deposited. Note that the integer amount includes the decimals of the coin.
   *
   * @return The amount of coins
   */
  public Unsigned256 amount() {
    return amount;
  }

  boolean isValid(BlockchainAddress receiver, Unsigned256 amount) {
    return receiver.equals(this.receiver) && Objects.equals(this.amount, amount);
  }

  /**
   * Register a signature for an oracle node.
   *
   * @param nodeIndex index of the oracle node
   * @return the deposit with the registered signature
   */
  public Deposit registerSignature(int nodeIndex) {
    if (hasSigned(nodeIndex)) {
      throw new IllegalStateException("Signature already received from node " + nodeIndex);
    }

    return new Deposit(receiver, amount, signaturesRegistered + (1 << nodeIndex));
  }

  /**
   * Check if an oracle node has signed.
   *
   * @param nodeIndex index of the node to check
   * @return true if node has signed
   */
  public boolean hasSigned(int nodeIndex) {
    return (this.signaturesRegistered & 1L << nodeIndex) != 0L;
  }

  /**
   * Determine if enough oracle nodes have signed for the deposit to be executed.
   *
   * @return True if at least two oracle nodes have signed
   */
  boolean hasEnoughSignatures() {
    return Integer.bitCount(signaturesRegistered) >= 2;
  }
}
