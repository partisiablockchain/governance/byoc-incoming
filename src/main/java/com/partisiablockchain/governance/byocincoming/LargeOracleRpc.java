package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.DataStreamSerializable;
import java.util.List;

final class LargeOracleRpc {
  static final long DISPUTE_TOKEN_COST = 2_500_0000L;
  static final long REQUIRED_ORACLE_STAKE = 250_000_0000L;
  static final byte DISPUTE_CREATE = 0;
  static final byte BURN_TOKENS = 3;
  static final byte REQUEST_NEW_SMALL_ORACLE = 5;
  static final byte RECALIBRATE_TOKENS = 8;
  static final byte CHECK_ORACLE_MEMBER_STATUS = 19;

  @SuppressWarnings("unused")
  private LargeOracleRpc() {}

  static DataStreamSerializable createDispute(BlockchainAddress challenger, long transactionNonce) {
    return stream -> {
      stream.writeByte(DISPUTE_CREATE);
      challenger.write(stream);
      stream.writeLong(DISPUTE_TOKEN_COST);
      stream.writeLong(0);
      stream.writeLong(transactionNonce);
      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_RESULT);
      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM);
    };
  }

  static DataStreamSerializable requestNewSmallOracle() {
    return stream -> {
      stream.writeByte(REQUEST_NEW_SMALL_ORACLE);
      stream.writeLong(REQUIRED_ORACLE_STAKE);
      stream.writeByte(ByocIncomingContract.Invocations.UPDATE_ORACLE);
      stream.writeDynamicBytes(new byte[0]);
    };
  }

  static DataStreamSerializable burnTokens(List<BlockchainAddress> responsibleOracles) {
    return stream -> {
      stream.writeByte(BURN_TOKENS);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, responsibleOracles);
      stream.writeLong(DISPUTE_TOKEN_COST);
    };
  }

  static DataStreamSerializable recalibrateTokens(
      String symbol, Unsigned256 amount, List<BlockchainAddress> responsibleOracles) {
    return stream -> {
      stream.writeByte(RECALIBRATE_TOKENS);
      stream.writeString(symbol);
      amount.write(stream);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, responsibleOracles);
    };
  }

  static DataStreamSerializable checkLoStatus(List<BlockchainAddress> oracleMembers) {
    return stream -> {
      stream.writeByte(CHECK_ORACLE_MEMBER_STATUS);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, oracleMembers);
    };
  }
}
