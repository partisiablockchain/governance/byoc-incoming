package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.InteractionBuilder;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A system contract that facilitates incoming BYOC deposits. When a coin from an external
 * blockchain is deposited to an external smart contract a twin BYOC coin is minted on PBC.
 *
 * <p>The BYOC deposit system is composed of
 *
 * <ol>
 *   <li>A smart contract on the external blockchain, where coins can be deposited.
 *   <li>This smart contract that keeps track of which nodes are allocated as the current BYOC
 *       incoming oracle and mints BYOC twins when enough oracle signatures are received.
 *   <li>The allocated BYOC incoming oracle nodes that run the byoc-service, which monitors the
 *       external chain and send transactions to this smart contract when a deposit occurs to the
 *       external smart contract.
 * </ol>
 */
@AutoSysContract(ByocIncomingContractState.class)
public final class ByocIncomingContract {

  private static final Logger logger = LoggerFactory.getLogger(ByocIncomingContract.class);

  static final int BYOC_TAX_PERMIL = 1;
  static final int DISPUTE_RESULT_FALSE_CLAIM = -1;
  static final int DISPUTE_RESULT_INSUFFICIENT_TOKENS = -2;

  static final long ONE_MONTH_MILLISECONDS = Duration.ofDays(28).toMillis();

  static final class Invocations {
    static final int DEPOSIT = 0;
    static final int DISPUTE_CREATE = 1;
    static final int DISPUTE_COUNTER_CLAIM = 2;
    static final int DISPUTE_RESULT = 3;
    static final int UPDATE_ORACLE = 4;
    static final int REQUEST_NEW_ORACLE = 5;

    private Invocations() {}
  }

  static final class CallBacks {

    static final int CHECK_MINTING_EVENT = 1;
    static final int REPLACE_NON_BP_SMALL_ORACLE = 2;

    private CallBacks() {}

    static DataStreamSerializable checkMintingEvent(
        List<BlockchainAddress> oracleNodes, Unsigned256 mintAmount) {
      return data -> {
        data.writeByte(CHECK_MINTING_EVENT);
        mintAmount.write(data);
        BlockchainAddress.LIST_SERIALIZER.writeDynamic(data, oracleNodes);
      };
    }

    static DataStreamSerializable replaceNonBpOracle(long oracleNonce) {
      return data -> {
        data.writeByte(REPLACE_NON_BP_SMALL_ORACLE);
        data.writeLong(oracleNonce);
      };
    }
  }

  /**
   * Initialize byoc incoming contract.
   *
   * @param context to invoke small oracle update
   * @param largeOracleContract address of the large oracle
   * @param symbol of the BYOC twin
   * @param maximumDepositPerEpoch maximum that can be deposited per epoch
   * @return initial state
   */
  @Init
  public ByocIncomingContractState create(
      SysContractContext context,
      BlockchainAddress largeOracleContract,
      String symbol,
      Unsigned256 maximumDepositPerEpoch) {
    ByocIncomingContractState state =
        new ByocIncomingContractState(
            largeOracleContract, symbol, List.of(), maximumDepositPerEpoch);
    InteractionBuilder invokeLargeOracle =
        context.getInvocationCreator().invoke(state.getLargeOracle());
    requestSmallOracleUpdate(invokeLargeOracle);
    return state;
  }

  /**
   * Sign a deposit that has been executed on the external chain.
   *
   * <p>This invocation is called by the allocated BYOC incoming oracle nodes, which monitors the
   * external chain and send this transaction when a deposit occurs to the external smart contract.
   *
   * <p>When enough agreeing signatures has been received twin BYOC coins are minted on PBC and
   * delivered to the receiver.
   *
   * <p>If conflicting deposits are signed by different oracle nodes the <i>dispute</i> process is
   * initiated. Disputes are settled by an automated vote performed by the large oracle, ie. the
   * entire block producer committee.
   *
   * <p><b>Pre:</b>
   *
   * <ul>
   *   <li>Can only be called by small oracle party.
   *   <li>An <var>amount</var> &gt; 0 is provided.
   *   <li><var>amount</var> &lt;= unused stakes of small oracle.
   *   <li>If no deposit operation is in progress, an unused deposit <var>nonce</var> is provided.
   *   <li>If a deposit operation is in progress, <var>nonce</var>, <var>amount</var> and
   *       <var>receiver</var> are identical to deposit in progress.
   * </ul>
   *
   * <p><b>Post:</b>
   *
   * <ul>
   *   <li>If deposit was not signed yet, sign deposit operation on behalf of small oracle party.
   *   <li>If signatures from enough small oracle parties are collected, create <var>amount</var>
   *       BYOC coins and clear deposit operation for given <var>nonce</var>.
   *   <li>Reduce remaining stakes of small oracle by <var>amount</var>.
   * </ul>
   *
   * <p><b>Abort:</b>
   *
   * <ul>
   *   <li>Deposited <var>amount</var> &gt; remaining stakes of small oracle then create new small
   *       oracle and restart process.
   *   <li>Deposited <var>amount</var> &gt; max stakes of small oracle then panic.
   *   <li>If deposit is in progress with agreeing nonce but differing amount or receiver, then
   *       start dispute.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param depositNonce unique identifier for the deposit
   * @param receiver the recipient of the deposit
   * @param amount to be deposited into the receiver
   * @return updated state
   */
  @Action(Invocations.DEPOSIT)
  public ByocIncomingContractState deposit(
      SysContractContext context,
      ByocIncomingContractState state,
      long depositNonce,
      BlockchainAddress receiver,
      Unsigned256 amount) {
    Unsigned256 maximumDepositPerEpoch = state.getMaximumDepositPerEpoch();
    ensure(
        amount.compareTo(maximumDepositPerEpoch) <= 0,
        "Deposited amount cannot exceed %s",
        maximumDepositPerEpoch);
    int nodeIndex = state.getNodeIndex(context.getFrom());
    Deposit deposit = state.getOrCreateDeposit(depositNonce, receiver, amount);

    if (!deposit.isValid(receiver, amount)) {
      Dispute.Transaction ethTx = new Dispute.Transaction(depositNonce, receiver, amount);
      return startDispute(context, state, ethTx);
    }

    ByocIncomingContractState updatedState = state;

    deposit = deposit.registerSignature(nodeIndex);
    updatedState = updatedState.setDeposit(deposit);

    if (deposit.hasEnoughSignatures()) {
      mintCoins(updatedState.getSymbol(), context, receiver, amount, state.getOracleNodes());
      updatedState = updatedState.clearDeposit();
      if (updatedState.getDepositSum().compareTo(maximumDepositPerEpoch) > 0) {
        updatedState = updatedState.endCurrentEpoch();
        InteractionBuilder invokeLargeOracle =
            context.getInvocationCreator().invoke(updatedState.getLargeOracle());
        requestSmallOracleUpdate(invokeLargeOracle);
      }
    }
    return updatedState;
  }

  /**
   * Creates a dispute for a given deposit.
   *
   * <p><b>Pre:</b>
   *
   * <ul>
   *   <li>Can only be called by large oracle on behalf of a dispute issuer, which can be any party.
   *   <li>Dispute issuer must have enough MPC tokens staked at large oracle.
   *   <li>Dispute is for a deposit event that exists on PBC blockchain.
   *   <li>An <var>amount</var> and a <var>receiver</var> are provided by dispute issuer.
   *   <li>Disputed deposit must differ in amount or receiver from <var>amount</var> or
   *       <var>receiver</var> provided by dispute issuer.
   *   <li>Deposit cannot already be disputed by dispute issuer.
   * </ul>
   *
   * <p><b>Post:</b>
   *
   * <ul>
   *   <li>The dispute is accepted and added.
   * </ul>
   *
   * <p><b>Abort:</b>
   *
   * <ul>
   *   <li>If dispute issuer does not have sufficient MPC tokens staked at large oracle, then
   *       dispute is not added.
   *   <li>If the dispute have been resolved, then the dispute is not added.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param transaction the transaction that should be disputed
   * @return updated state
   */
  @Action(Invocations.DISPUTE_CREATE)
  public ByocIncomingContractState disputeCreate(
      SysContractContext context,
      ByocIncomingContractState state,
      Dispute.Transaction transaction) {

    long depositNonce = transaction.nonce();
    Deposit deposit = state.getDeposits().getValue(depositNonce);
    ensure(deposit != null, "No deposit with nonce %d found", depositNonce);

    boolean invalidDispute =
        deposit.receiver().equals(transaction.receiver())
            && Objects.equals(deposit.amount(), transaction.amount());
    ensure(!invalidDispute, "Disputed transaction is equal to the one stored in PBC state");

    ensure(
        transaction.amount().compareTo(Unsigned256.ZERO) > 0,
        "Cannot create a dispute for the amount zero");

    boolean depositHasBeenDisputed = state.hasDepositBeenDisputed(depositNonce);
    ensure(!depositHasBeenDisputed, "Disputed deposit has already been resolved");

    return startDispute(context, state, transaction);
  }

  /**
   * Counter a dispute made during a deposit call, where the deposit was deemed invalid and
   * disputed. Only the large oracle can make a counter-claim to a dispute.
   *
   * <p><b>Pre:</b>
   *
   * <ul>
   *   <li>Can only be called through large oracle by a large oracle party.
   *   <li>Event must contain a <var>deposit</var> that exists on PBC.
   *   <li>There exists a dispute for <var>deposit</var> that are currently in progress and voting
   *       has not finished yet.
   *   <li>Counter-claim creator has not made counter-claim for this deposit before.
   * </ul>
   *
   * <p><b>Post:</b>
   *
   * <ul>
   *   <li>Added counter-claim on behalf of party to dispute.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param transaction the transaction that are being disputed
   * @return update state
   */
  @Action(Invocations.DISPUTE_COUNTER_CLAIM)
  public ByocIncomingContractState disputeCounterClaim(
      SysContractContext context,
      ByocIncomingContractState state,
      Dispute.Transaction transaction) {
    BlockchainAddress largeOracle = state.getLargeOracle();
    BlockchainAddress sender = context.getFrom();
    ensure(
        largeOracle.equals(sender),
        "Counter-claims can only be added through the large oracle contract");
    ensure(
        state.getDisputes().getValue(transaction.nonce()) != null,
        "No dispute to add counter-claim to");
    ensure(
        state.getDisputes().getValue(transaction.nonce()).getVotingResult() == null,
        "Cannot add counter-claim to resolved dispute");
    return state.addCounterClaim(transaction);
  }

  /**
   * Resolves the dispute and handles a fraudulent deposit accordingly. A dispute can only be
   * resolved by the large oracle.
   *
   * <p><b>Pre:</b>
   *
   * <ul>
   *   <li>Can only be called by large oracle contract.
   *   <li>Event must contain a <var>deposit</var> that exists on PBC.
   *   <li>Deposit must currently be in dispute and the dispute is not resolved yet.
   *   <li>At least 2/3rds of the large oracle parties have voted concerning this deposit.
   *   <li>If majority approved a different deposit, then this deposit must be provided by event.
   * </ul>
   *
   * <p><b>Post:</b>
   *
   * <ul>
   *   <li>If large oracle decides that there was a problematic deposit, then vote for new small
   *       oracle.
   *   <li>If claim was baseless, then punish dispute issuer.
   *   <li>If claim was not baseless, then punish small oracle parties that created deposit as
   *       follows:
   *       <ul>
   *         <li><strong>Type 1-fraud</strong>: If no deposit was issued, deposit was issued to
   *             wrong receiver or receiver obtained too little, then mint new BYOC coins for
   *             receiver.
   *         <li><strong>Type 2-fraud</strong>: If receiver obtained too many BYOC tokens, then sell
   *             off difference in MPC tokens for ETH.
   *         <li><strong>Type 2-fraud</strong>: If wrong receiver obtained BYOC tokens, then sell
   *             off MPC tokens for ETH and create appropriately many BYOC tokens for receiver.
   *       </ul>
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param oracleNonce unique identifier of the oracle
   * @param disputeNonce unique identifier of the dispute
   * @param disputeResult result of dispute
   * @return updated state
   */
  @Action(Invocations.DISPUTE_RESULT)
  public ByocIncomingContractState disputeResult(
      SysContractContext context,
      ByocIncomingContractState state,
      long oracleNonce,
      long disputeNonce,
      int disputeResult) {
    BlockchainAddress largeOracle = state.getLargeOracle();
    BlockchainAddress sender = context.getFrom();
    ensure(largeOracle.equals(sender), "Dispute can only be resolved by the large oracle contract");

    Dispute dispute = state.getDisputes().getValue(disputeNonce);
    ensure(
        dispute != null && dispute.getVotingResult() == null,
        "Dispute does not exists or has already been resolved");
    InteractionBuilder invokeLargeOracle = context.getInvocationCreator().invoke(largeOracle);

    if (disputeResult < 0) {
      if (disputeResult == DISPUTE_RESULT_INSUFFICIENT_TOKENS) {
        BlockchainAddress challenger = dispute.getChallenger();
        logger.info(
            "Challenger {} had insufficient tokens staked to large oracle to start a dispute",
            challenger);
        return state.dropDispute(disputeNonce);
      }
      return state.archiveDispute(disputeNonce, disputeResult);
    } else {
      Dispute.Transaction transaction = dispute.getClaims().get(disputeResult);
      return handleFraudulentClaim(state, context, transaction, invokeLargeOracle)
          .archiveDispute(disputeNonce, disputeResult);
    }
  }

  /**
   * Update the small oracle with new addresses. Reads the public keys of the parties, but does not
   * use them.
   *
   * <p><b>Pre:</b>
   *
   * <ul>
   *   <li>Can only be called by large oracle.
   *   <li>Currently, the small oracle consists of parties <var>S</var>.
   *   <li>Each party in <var>S</var> has tokens staked to small oracle.
   *   <li>Each party that could be elected as small oracle party has =&gt; max tokens associated
   *       free with small oracle.
   * </ul>
   *
   * <p><b>Post:</b>
   *
   * <ul>
   *   <li>A new small oracle with parties <var>S'</var> chosen.
   *   <li>All parties who previously had <var>y</var> staked tokens with small oracle set a
   *       associated pending and <var>z</var> as associated free now have 0 as associated pending
   *       and <var>y+z</var> as associated free.
   *   <li>All parties in <var>S</var> have their staked tokens for small oracle released to be
   *       associated pending.
   *   <li>All parties in <var>S'</var> have staked <var>x</var> tokens staked to small oracle.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param newOracles the list of addresses to use as oracle
   * @param oracleKeys the list of public keys to use as oracle
   * @return updated state
   */
  @Action(Invocations.UPDATE_ORACLE)
  public ByocIncomingContractState updateOracle(
      SysContractContext context,
      ByocIncomingContractState state,
      List<BlockchainAddress> newOracles,
      List<BlockchainPublicKey> oracleKeys) {
    BlockchainAddress from = context.getFrom();
    ensure(
        from.equals(state.getLargeOracle()),
        "Updates to the small oracle can only be done through the large oracle contract");
    return state.startNewEpoch(newOracles, context.getBlockProductionTime());
  }

  /**
   * Request selection of a new small oracle.
   *
   * <p>Criteria for oracle rotation:
   *
   * <ul>
   *   <li>Governance contracts can rotate the small oracle at any time.
   *   <li>Current members of the small oracle can rotate the small oracle if 28 days have passed
   *       since the current small oracle was started.
   *   <li>Anyone can rotate the small oracle if any member of the current oracle is not a block
   *       producer.
   *   <li>The small oracle cannot be rotated if a small oracle change is already in progress.
   * </ul>
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return updated state if new oracle is requested
   */
  @Action(Invocations.REQUEST_NEW_ORACLE)
  public ByocIncomingContractState requestNewOracle(
      SysContractContext context, ByocIncomingContractState state) {
    BlockchainAddress sender = context.getFrom();
    ensure(
        !state.getOracleNodes().isEmpty(),
        "Cannot request new small oracle during small oracle change");
    long currentTime = context.getBlockProductionTime();
    boolean oneMonthHasPassed = currentTime - state.getOracleTimestamp() >= ONE_MONTH_MILLISECONDS;
    boolean senderIsOracleMember = state.getOracleNodes().contains(sender);
    if (sender.getType() == BlockchainAddress.Type.CONTRACT_GOV
        || (oneMonthHasPassed && senderIsOracleMember)) {
      return requestOracleChange(context, state);
    } else {
      checkLargeOracleStatusOfSmallOracleMembers(context, state);
      return state;
    }
  }

  /**
   * End current epoch and request a new small oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @return state with new epoch
   */
  private static ByocIncomingContractState requestOracleChange(
      SysContractContext context, ByocIncomingContractState state) {
    ByocIncomingContractState updatedState = state.endCurrentEpoch();
    InteractionBuilder invokeLargeOracle =
        context.getInvocationCreator().invoke(updatedState.getLargeOracle());
    requestSmallOracleUpdate(invokeLargeOracle);
    return updatedState;
  }

  /**
   * Check with large oracle if all small oracle members are large oracle members. Requests a new
   * small oracle if any member is found not to be part of large oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   */
  private static void checkLargeOracleStatusOfSmallOracleMembers(
      SysContractContext context, ByocIncomingContractState state) {
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        CallBacks.replaceNonBpOracle(state.getOracleNonce()));
    eventManager
        .invoke(state.getLargeOracle())
        .withPayload(LargeOracleRpc.checkLoStatus(state.getOracleNodes()))
        .sendFromContract();
  }

  /**
   * Request a new small oracle if at least one member is not part of large oracle.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param oracleNonce nonce of the oracle to be replaced
   * @return updated state
   */
  @Callback(CallBacks.REPLACE_NON_BP_SMALL_ORACLE)
  public ByocIncomingContractState replaceNonBpOracleCallback(
      SysContractContext context,
      ByocIncomingContractState state,
      CallbackContext callbackContext,
      Long oracleNonce) {
    boolean allMembersArePartOfLargeOracle =
        callbackContext.results().get(0).returnValue().readBoolean();
    boolean oracleHasNotChanged = state.getOracleNonce() == oracleNonce;
    ensure(oracleHasNotChanged, "Oracle with nonce %d has already been changed", oracleNonce);
    ensure(
        !allMembersArePartOfLargeOracle,
        "Cannot replace non-BP oracle as all oracle members of nonce %d are block producers",
        oracleNonce);

    return requestOracleChange(context, state);
  }

  /**
   * Checks if the minting of byoc twins was successful.
   *
   * @param context the contract context containing sender and chain information
   * @param state the current state of the contract
   * @param callbackContext information about callbacks
   * @param amount possible minted amount of byoc twin
   * @param oracles oracles responsible for minting event
   * @return updated state
   */
  @Callback(CallBacks.CHECK_MINTING_EVENT)
  public ByocIncomingContractState checkMintingEvent(
      SysContractContext context,
      ByocIncomingContractState state,
      CallbackContext callbackContext,
      Unsigned256 amount,
      List<BlockchainAddress> oracles) {
    if (!callbackContext.isSuccess()) {
      // Minting event failed.
      // Ensure that BYOC twins are not account for by registering them as service fees.
      context.registerDeductedByocFees(amount, state.getSymbol(), FixedList.create(oracles));
    }
    return state;
  }

  private static ByocIncomingContractState startDispute(
      SysContractContext context, ByocIncomingContractState state, Dispute.Transaction externalTx) {
    BlockchainAddress challenger = context.getFrom();
    Dispute dispute = Dispute.create(challenger, externalTx);

    context
        .getInvocationCreator()
        .invoke(state.getLargeOracle())
        .withPayload(LargeOracleRpc.createDispute(challenger, externalTx.nonce()))
        .sendFromContract();

    return state.addDispute(dispute);
  }

  private static ByocIncomingContractState handleFraudulentClaim(
      ByocIncomingContractState oldState,
      SysContractContext context,
      Dispute.Transaction transaction,
      InteractionBuilder invokeLargeOracle) {
    ByocIncomingContractState updatedState = oldState;

    long depositNonce = transaction.nonce();
    long oracleNonce = oldState.getOracleNonceForDepositNonce(depositNonce);
    if (oracleNonce == oldState.getOracleNonce()) {
      updatedState = updatedState.endCurrentEpoch();
      requestSmallOracleUpdate(invokeLargeOracle);
    }

    List<BlockchainAddress> epochOracles =
        updatedState.getEpochs().getValue(oracleNonce).getOracles();
    Deposit deposit;
    boolean isCurrentDeposit = depositNonce == oldState.getDepositNonce();
    if (isCurrentDeposit) {
      deposit = updatedState.getDeposit();
    } else {
      deposit = updatedState.getDeposits().getValue(depositNonce);
    }
    List<BlockchainAddress> responsibleOracles = getResponsibleOracles(epochOracles, deposit);

    // Penalize oracles for cheating
    invokeLargeOracle.withPayload(LargeOracleRpc.burnTokens(responsibleOracles)).sendFromContract();

    if (isCurrentDeposit) {
      mintCoins(
          updatedState.getSymbol(),
          context,
          transaction.receiver(),
          transaction.amount(),
          List.of());
      return updatedState.clearDeposit();
    }

    BlockchainAddress receiver = transaction.receiver();
    String symbol = updatedState.getSymbol();
    if (isType1Fraud(deposit, transaction)) { // We are able to mint coins for the correct receiver
      Unsigned256 amount;
      if (!deposit.receiver().equals(receiver)) {
        amount = transaction.amount();
      } else {
        amount = transaction.amount().subtract(deposit.amount());
      }
      // Taxes deducted from minting the coins should not be given to the cheating oracles.
      // Instead, they are all registered as blockchain usage.
      mintCoins(symbol, context, receiver, amount, Collections.emptyList());
    }

    // Confiscate and sell off mpc tokens for excess BYOC twins.
    if (isType2Fraud(deposit, transaction)) {
      Unsigned256 amount;
      if (!deposit.receiver().equals(receiver)) {
        amount = deposit.amount();
      } else {
        amount = deposit.amount().subtract(transaction.amount());
      }
      invokeLargeOracle
          .withPayload(LargeOracleRpc.recalibrateTokens(symbol, amount, responsibleOracles))
          .sendFromContract();
    }

    return updatedState;
  }

  private static List<BlockchainAddress> getResponsibleOracles(
      List<BlockchainAddress> epochOracles, Deposit deposit) {
    List<BlockchainAddress> result = new ArrayList<>();
    for (BlockchainAddress candidate : epochOracles) {
      int index = epochOracles.indexOf(candidate);
      if (deposit.hasSigned(index)) {
        result.add(candidate);
      }
    }
    return result;
  }

  private static void requestSmallOracleUpdate(InteractionBuilder invokeLargeOracle) {
    invokeLargeOracle.withPayload(LargeOracleRpc.requestNewSmallOracle()).sendFromContract();
  }

  static boolean isType1Fraud(Deposit deposit, Dispute.Transaction transaction) {
    if (deposit == null) {
      return true;
    } else {
      return deposit.amount().compareTo(transaction.amount()) < 0
          || !deposit.receiver().equals(transaction.receiver());
    }
  }

  static boolean isType2Fraud(Deposit deposit, Dispute.Transaction transaction) {
    if (deposit != null) {
      return deposit.amount().compareTo(transaction.amount()) > 0
          || !deposit.receiver().equals(transaction.receiver());
    } else {
      return false;
    }
  }

  private static void mintCoins(
      String symbol,
      SysContractContext context,
      BlockchainAddress receiver,
      Unsigned256 amount,
      List<BlockchainAddress> oracleNodes) {
    logger.debug("Executing deposit: {} {} to {}", amount, symbol, receiver);
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    Unsigned256 tax =
        amount.multiply(Unsigned256.create(BYOC_TAX_PERMIL)).divide(Unsigned256.create(1000));
    context.registerDeductedByocFees(tax, symbol, FixedList.create(oracleNodes));

    Unsigned256 mintAmount = amount.subtract(tax);
    eventManager.registerCallbackWithCostFromRemaining(
        CallBacks.checkMintingEvent(oracleNodes, mintAmount));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            receiver, AccountPluginRpc.addCoinBalance(symbol, mintAmount)));
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public ByocIncomingContractState upgrade(StateAccessor oldState) {
    return ByocIncomingContractState.migrateState(oldState);
  }

  @FormatMethod
  private static void ensure(boolean predicate, String messageFormat, Object... args) {
    if (!predicate) {
      String errorMessage = String.format(messageFormat, args);
      throw new RuntimeException(errorMessage);
    }
  }
}
