package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/** State for incoming BYOC deposits. */
@Immutable
public final class ByocIncomingContractState implements StateSerializable {

  /**
   * The large oracle contract. The large oracle allocates nodes to the BYOC oracle and handles
   * disputes.
   */
  private final BlockchainAddress largeOracle;

  /**
   * Nonce of the current deposit being processed. Ensures that deposits are processed in strict
   * sequence.
   */
  private final long depositNonce;

  /** All deposits that have been completed. */
  private final AvlTree<Long, Deposit> deposits;

  /** The PBC symbol of the coin that this contract handles. */
  private final String symbol;

  /**
   * The nodes that are currently acting as the BYOC incoming oracle. These nodes monitor the BYOC
   * contract on the external chain and whenever a deposit is made to it they send a transaction to
   * this contract.
   */
  private final FixedList<BlockchainAddress> oracleNodes;

  /**
   * The current deposit being processed. Null if no oracle node has yet sent information about the
   * deposit. When the deposit receives two signatures it is executed and the processing is
   * complete.
   */
  private final Deposit deposit;

  /**
   * The sum of all deposits completed by the current oracle nodes. This is used ensure the oracle
   * nodes have sufficient collateral. It is reset when a new oracle is selected.
   */
  private final Unsigned256 depositSum;

  /**
   * The maximal amount that can be deposited before the BYOC oracle nodes must be rotated. The
   * period of each BYOC oracle is referred to as an epoch.
   */
  private final Unsigned256 maximumDepositPerEpoch;

  /**
   * Disputes for deposits, that are used to settle disagreements. The key is the deposit nonce of
   * the disputed deposit.
   */
  private final AvlTree<Long, Dispute> disputes;

  /**
   * The ID of the current serving BYOC deposit oracle. Incremented every time a new oracle is
   * allocated.
   */
  private final long oracleNonce;

  /**
   * Keeps track of all historic BYOC deposit oracles. The key in the map is the oracle nonce, ie.
   * the oracle ID .
   */
  private final AvlTree<Long, Epoch> epochs;

  /** The time when the current serving oracle was allocated. Unix time in milliseconds. */
  private final long oracleTimestamp;

  @SuppressWarnings("unused")
  ByocIncomingContractState() {
    largeOracle = null;
    depositNonce = 0L;
    deposits = null;
    symbol = null;
    oracleNodes = null;
    deposit = null;
    depositSum = null;
    maximumDepositPerEpoch = null;
    disputes = null;
    oracleNonce = 0L;
    epochs = null;
    oracleTimestamp = 0L;
  }

  /**
   * Default constructor for the ByocIncomingContractState.
   *
   * @param largeOracle address for the large oracle contract
   * @param symbol symbol of the BYOC twin
   * @param oracleNodes list of oracle addresses
   * @param maximumDepositPerEpoch the maximum allowed deposit amount per epoch
   */
  public ByocIncomingContractState(
      BlockchainAddress largeOracle,
      String symbol,
      List<BlockchainAddress> oracleNodes,
      Unsigned256 maximumDepositPerEpoch) {
    this(
        largeOracle,
        0L,
        AvlTree.create(),
        symbol,
        FixedList.create(oracleNodes),
        null,
        Unsigned256.ZERO,
        maximumDepositPerEpoch,
        AvlTree.create(),
        0L,
        AvlTree.create(),
        0);
  }

  private ByocIncomingContractState(
      BlockchainAddress largeOracle,
      long depositNonce,
      AvlTree<Long, Deposit> deposits,
      String symbol,
      FixedList<BlockchainAddress> oracleNodes,
      Deposit deposit,
      Unsigned256 depositSum,
      Unsigned256 maximumDepositPerEpoch,
      AvlTree<Long, Dispute> disputes,
      long oracleNonce,
      AvlTree<Long, Epoch> epochs,
      long oracleTimestamp) {
    this.largeOracle = largeOracle;
    this.depositNonce = depositNonce;
    this.deposits = deposits;
    this.symbol = symbol;
    this.oracleNodes = oracleNodes;
    this.deposit = deposit;
    this.depositSum = depositSum;
    this.maximumDepositPerEpoch = maximumDepositPerEpoch;
    this.disputes = disputes;
    this.oracleNonce = oracleNonce;
    this.epochs = epochs;
    this.oracleTimestamp = oracleTimestamp;
  }

  static ByocIncomingContractState migrateState(StateAccessor oldState) {
    BlockchainAddress largeOracle = oldState.get("largeOracle").blockchainAddressValue();
    String symbol = oldState.get("symbol").stringValue();

    List<BlockchainAddress> oracleNodes =
        oldState.get("oracleNodes").getListElements().stream()
            .map(StateAccessor::blockchainAddressValue)
            .toList();

    long depositNonce = oldState.get("depositNonce").longValue();
    AvlTree<Long, Deposit> deposits =
        migrateAvlTree(
            oldState.get("deposits"), StateAccessor::longValue, Deposit::fromStateAccessor);
    Unsigned256 depositSum = oldState.get("depositSum").cast(Unsigned256.class);
    Unsigned256 maximumDepositPerEpoch =
        oldState.get("maximumDepositPerEpoch").cast(Unsigned256.class);
    Deposit deposit = Deposit.fromStateAccessor(oldState.get("deposit"));

    AvlTree<Long, Dispute> disputes =
        migrateAvlTree(
            oldState.get("disputes"), StateAccessor::longValue, Dispute::fromStateAccessor);
    long oracleNonce = oldState.get("oracleNonce").longValue();

    AvlTree<Long, Epoch> epochs =
        migrateAvlTree(oldState.get("epochs"), StateAccessor::longValue, Epoch::fromStateAccessor);

    long oracleTimestamp = oldState.get("oracleTimestamp").longValue();

    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        FixedList.create(oracleNodes),
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes,
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  private static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> migrateAvlTree(
          StateAccessor accessor,
          Function<StateAccessor, K> getKey,
          Function<StateAccessor, V> getValue) {
    AvlTree<K, V> result = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      K key = getKey.apply(entry.getKey());
      V value = getValue.apply(entry.getValue());
      result = result.set(key, value);
    }

    return result;
  }

  /**
   * Get the large oracle contract.
   *
   * @return address of large oracle
   */
  public BlockchainAddress getLargeOracle() {
    return largeOracle;
  }

  /**
   * Get the nonce of the current deposit being processed.
   *
   * @return The current deposit nonce
   */
  public long getDepositNonce() {
    return depositNonce;
  }

  /**
   * Get all deposits.
   *
   * @return map from deposit nonce to deposit state
   */
  public AvlTree<Long, Deposit> getDeposits() {
    return deposits;
  }

  /**
   * Get the PBC symbol of the coin that this contract handles.
   *
   * @return coin symbol
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * Get the nodes that are currently acting as the BYOC incoming oracle.
   *
   * @return addresses of the current oracle nodes
   */
  public List<BlockchainAddress> getOracleNodes() {
    return new ArrayList<>(oracleNodes);
  }

  /**
   * Get the current deposit being processed. Null if no oracle node has yet sent information about
   * the deposit.
   *
   * @return The current deposit
   */
  public Deposit getDeposit() {
    return deposit;
  }

  /**
   * Get the sum of all deposits completed by the current oracle nodes. This is used ensure the
   * oracle nodes have sufficient collateral. It is reset when a new oracle is selected.
   *
   * @return The deposit sum
   */
  public Unsigned256 getDepositSum() {
    return depositSum;
  }

  /**
   * Get The maximal amount that can be deposited before the BYOC oracle nodes must be rotated. The
   * period of each BYOC oracle is referred to as an epoch.
   *
   * @return The maximum allowed deposit per epoch
   */
  public Unsigned256 getMaximumDepositPerEpoch() {
    return maximumDepositPerEpoch;
  }

  /**
   * Get the ID of the current serving BYOC deposit oracle.
   *
   * @return nonce of the current oracle
   */
  public long getOracleNonce() {
    return oracleNonce;
  }

  /**
   * Get all historic BYOC deposit oracles. The key in the map is the oracle nonce, ie. the oracle
   * ID .
   *
   * @return Map from oracle nonce to epoch
   */
  public AvlTree<Long, Epoch> getEpochs() {
    return epochs;
  }

  Deposit getOrCreateDeposit(long depositNonce, BlockchainAddress receiver, Unsigned256 amount) {
    if (this.depositNonce != depositNonce) {
      throw new IllegalStateException("Invalid deposit nonce");
    }
    if (deposit != null) {
      return deposit;
    }
    if (amount.compareTo(Unsigned256.ZERO) == 0) {
      throw new IllegalStateException("Invalid amount. Amount should be greater than zero");
    } else {
      return new Deposit(receiver, amount);
    }
  }

  /**
   * Get all disputes for deposits. Disputes are used to settle disagreements.
   *
   * @return map from the deposit nonce of the disputed deposit to the dispute status.
   */
  public AvlTree<Long, Dispute> getDisputes() {
    return disputes;
  }

  /**
   * Check if a deposit has been disputed.
   *
   * @param depositNonce nonce of deposit to check
   * @return true if deposit has been disputed
   */
  public boolean hasDepositBeenDisputed(long depositNonce) {
    return disputes.containsKey(depositNonce);
  }

  /**
   * Get the nonce of the oracle that was (or is) responsible for the given deposit nonce.
   *
   * @param depositNonce nonce of the deposit to get oracles for
   * @return nonce of the oracle responsible for depositNonce, or -1 if no oracle was found
   */
  public long getOracleNonceForDepositNonce(long depositNonce) {
    if (epochs.size() > 0) {
      Epoch lastEpoch = epochs.getValue(oracleNonce - 1);
      if (lastEpoch.getToDepositNonce() <= depositNonce) {
        return oracleNonce;
      }
      for (Long oracleNonce : epochs.keySet()) {
        Epoch epoch = epochs.getValue(oracleNonce);
        if (epoch.getFromDepositNonce() <= depositNonce
            && depositNonce < epoch.getToDepositNonce()) {
          return oracleNonce;
        }
      }
      return -1L;
    } else {
      return 0L;
    }
  }

  ByocIncomingContractState setDeposit(Deposit deposit) {
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        oracleNodes,
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes,
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  ByocIncomingContractState clearDeposit() {
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce + 1,
        deposits.set(depositNonce, deposit),
        symbol,
        oracleNodes,
        null,
        depositSum.add(deposit.amount()),
        maximumDepositPerEpoch,
        disputes,
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  ByocIncomingContractState addDispute(Dispute dispute) {
    long nonce = dispute.getOriginalClaim().nonce();
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        oracleNodes,
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes.set(nonce, dispute),
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  ByocIncomingContractState dropDispute(long nonce) {
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        oracleNodes,
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes.remove(nonce),
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  ByocIncomingContractState addCounterClaim(Dispute.Transaction transaction) {
    long nonce = transaction.nonce();
    Dispute dispute = disputes.getValue(nonce).addCounterClaim(transaction);
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        oracleNodes,
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes.set(nonce, dispute),
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  ByocIncomingContractState archiveDispute(long nonce, int votingResult) {
    Dispute dispute = disputes.getValue(nonce).addVotingResult(votingResult);
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        oracleNodes,
        deposit,
        depositSum,
        maximumDepositPerEpoch,
        disputes.set(nonce, dispute),
        oracleNonce,
        epochs,
        oracleTimestamp);
  }

  /**
   * End the current epoch. Disable transactions, until the oracles have been updated by the large
   * oracle.
   *
   * @return a state with a new epoch and no active oracle nodes
   */
  public ByocIncomingContractState endCurrentEpoch() {
    long epochStart;
    if (oracleNonce > 0) {
      epochStart = epochs.getValue(oracleNonce - 1L).getToDepositNonce();
    } else {
      epochStart = 0L;
    }
    Epoch epoch = new Epoch(epochStart, depositNonce, oracleNodes);
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        FixedList.create(),
        deposit,
        Unsigned256.ZERO,
        maximumDepositPerEpoch,
        disputes,
        oracleNonce + 1,
        epochs.set(oracleNonce, epoch),
        oracleTimestamp);
  }

  /**
   * Start new epoch by updating the oracle nodes.
   *
   * @param newOracles the new oracles nodes
   * @param timestamp timestamp of new oracle creation
   * @return a new state with new oracles nodes
   */
  public ByocIncomingContractState startNewEpoch(
      List<BlockchainAddress> newOracles, long timestamp) {
    return new ByocIncomingContractState(
        largeOracle,
        depositNonce,
        deposits,
        symbol,
        FixedList.create(newOracles),
        null,
        depositSum,
        maximumDepositPerEpoch,
        disputes,
        oracleNonce,
        epochs,
        timestamp);
  }

  /**
   * Get the time when the current serving oracle was allocated.
   *
   * @return Unix timestamp in milliseconds.
   */
  public long getOracleTimestamp() {
    return oracleTimestamp;
  }

  /**
   * Get the index of a node in the current oracle.
   *
   * @param from The oracle node to find the index for
   * @return The index of the oracle node.
   * @throws IllegalStateException if the node is not part of the current oracle.
   */
  int getNodeIndex(BlockchainAddress from) {
    int nodeIndex = oracleNodes.indexOf(from);
    if (nodeIndex == -1) {
      throw new IllegalStateException("Transaction did not come from an oracle node");
    }
    return nodeIndex;
  }
}
