package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ByocIncomingContractTest {

  private final SysContractSerialization<ByocIncomingContractState> serialization =
      new SysContractSerialization<>(
          ByocIncomingContractInvoker.class, ByocIncomingContractState.class);

  private final KeyPair oracleKey0 = new KeyPair(BigInteger.ONE);
  private final KeyPair oracleKey1 = new KeyPair(BigInteger.TWO);
  private final BlockchainAddress oracle0 =
      BlockchainAddress.fromString("000000000000000000000000000000000000001111");
  private final BlockchainAddress oracle1 =
      BlockchainAddress.fromString("000000000000000000000000000000000000002222");
  private final BlockchainAddress oracle2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000003333");
  private final List<BlockchainAddress> oracles = List.of(oracle0, oracle1, oracle2);
  private final BlockchainAddress voting =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeString("Vote")));

  private static final String symbol = "TEST_SYMBOL";
  private static final Unsigned256 amount = Unsigned256.create(123456);
  private static final Unsigned256 maxDeposit = Unsigned256.create(250_000L);
  private final BlockchainAddress largeOracle =
      BlockchainAddress.fromString("010000000000000000000000000000000000001234");
  private final BlockchainAddress receiver =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress receiver2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000012");
  private final BlockchainAddress receiver3 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000013");

  private ByocIncomingContractState state;
  private SysContractContextTest context;
  private static final byte INVALID_INVOCATION_BYTE = 100;

  /** Set-up. */
  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest(0, 100, receiver);
    state = new ByocIncomingContractState(largeOracle, symbol, oracles, maxDeposit);
  }

  @Test
  public void create() {
    state =
        serialization.create(
            context,
            stream -> {
              largeOracle.write(stream);
              stream.writeString(symbol);
              maxDeposit.write(stream);
            });
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    validateNewSmallOracleRequestWasSent(actualRpc);
    assertThat(state.getSymbol()).isEqualTo(symbol);
    assertThat(state.getOracleNodes()).isEmpty();
  }

  /** A governance contract can request a new oracle. */
  @Test
  public void requestNewOracleAsVotingContract() {
    ByocIncomingContractState stateWithTimestamp = state.startNewEpoch(oracles, 0);
    assertThat(stateWithTimestamp.getOracleNonce()).isEqualTo(0);
    newContextWithSender(voting);
    ByocIncomingContractState requestedNewOracle =
        serialization.invoke(
            context,
            stateWithTimestamp,
            rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE));
    // Ensure old oracle was removed, despite being less than 28 days old
    assertThat(requestedNewOracle.getOracleNonce()).isEqualTo(1);
  }

  /** If one month has passed an oracle member can request a new oracle. */
  @Test
  public void requestNewOracle() {
    ByocIncomingContractState stateWithTimestamp = state.startNewEpoch(oracles, 10);
    assertThat(stateWithTimestamp.getOracleTimestamp()).isEqualTo(10);
    setBlockProductionTime(ByocIncomingContract.ONE_MONTH_MILLISECONDS + 10);
    newContextWithSender(oracle1);
    ByocIncomingContractState requestedNewOracle =
        serialization.invoke(
            context,
            stateWithTimestamp,
            rpc -> {
              rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE);
            });
    assertThat(requestedNewOracle.getOracleNodes()).isEmpty();
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle());
    assertThat(actualRpc).isEqualTo(expectedRpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    assertThat(input.readUnsignedByte()).isEqualTo(LargeOracleRpc.REQUEST_NEW_SMALL_ORACLE);
    assertThat(input.readLong()).isEqualTo(LargeOracleRpc.REQUIRED_ORACLE_STAKE);
    assertThat(input.readUnsignedByte()).isEqualTo(ByocIncomingContract.Invocations.UPDATE_ORACLE);
    assertThat(input.readDynamicBytes()).isEqualTo(new byte[0]);
  }

  /**
   * If an oracle member requests a new oracle before one month has passed the contract will attempt
   * to replace non-BP oracle.
   */
  @Test
  public void requestNewOracleBeforeOneMonth() {
    newContextWithSender(oracle1);
    ByocIncomingContractState stateWithTimestamp = state.startNewEpoch(oracles, 10);
    assertThat(stateWithTimestamp.getOracleTimestamp()).isEqualTo(10);
    setBlockProductionTime(ByocIncomingContract.ONE_MONTH_MILLISECONDS);
    serialization.invoke(
        context,
        stateWithTimestamp,
        rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE));
    byte[] actualRpc = SafeDataOutputStream.serialize(getRemoteCalls().get(0).rpc::write);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(context.getRemoteCalls().callbackRpc[0])
        .isEqualTo((byte) ByocIncomingContract.CallBacks.REPLACE_NON_BP_SMALL_ORACLE);
  }

  /**
   * If a non-oracle member requests a new oracle after one month has passed the contract will
   * attempt to replace non-BP oracle.
   */
  @Test
  public void requestNewOracleNotSmallOracleMemberCallAfterOneMonth() {
    ByocIncomingContractState stateWithTimestamp = state.startNewEpoch(oracles, 10);
    assertThat(stateWithTimestamp.getOracleTimestamp()).isEqualTo(10);
    setBlockProductionTime(ByocIncomingContract.ONE_MONTH_MILLISECONDS + 10);
    serialization.invoke(
        context,
        stateWithTimestamp,
        rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE));
    byte[] actualRpc = SafeDataOutputStream.serialize(s -> getRemoteCalls().get(0).rpc.write(s));
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(context.getRemoteCalls().callbackRpc[0])
        .isEqualTo((byte) ByocIncomingContract.CallBacks.REPLACE_NON_BP_SMALL_ORACLE);
  }

  /** If small oracle contains non-BP member anyone can request a new small oracle. */
  @Test
  public void replaceNonBpOracle() {
    BlockchainAddress nonOracleAccount =
        BlockchainAddress.fromString("000000000000000000000000000000000123450011");
    newContextWithSender(nonOracleAccount);
    ByocIncomingContractState previousState = state;
    ByocIncomingContractState invokedState =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE));
    assertThat(invokedState).usingRecursiveComparison().isEqualTo(previousState);
    byte[] actualRpc = SafeDataOutputStream.serialize(getRemoteCalls().get(0).rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.checkLoStatus(oracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    SafeDataInputStream input = SafeDataInputStream.createFromBytes(actualRpc);
    assertThat(input.readUnsignedByte()).isEqualTo(LargeOracleRpc.CHECK_ORACLE_MEMBER_STATUS);
    assertThat(input.readInt()).isEqualTo(oracles.size());
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle0);
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle1);
    assertThat(BlockchainAddress.read(input)).isEqualTo(oracle2);
  }

  /** If there is currently an ongoing oracle change a new oracle cannot be requested. */
  @Test
  public void requestOracleOngoingOracleChange() {
    newContextWithSender(oracle0);
    setBlockProductionTime(
        ByocIncomingContract.ONE_MONTH_MILLISECONDS + state.getOracleTimestamp());
    ByocIncomingContractState waitingForOracleState =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE));
    assertThat(waitingForOracleState.getOracleNodes()).isEmpty();

    assertThatThrownBy(
        () ->
            serialization.invoke(
                context,
                waitingForOracleState,
                rpc -> rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE)),
        "Cannot request new small oracle during small oracle change");
  }

  /** If small oracle is found to contain a non-BP oracle member a new small oracle is requested. */
  @Test
  public void replaceNonBpOracleCallbackChangeOracle() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(false);
    callbackContext.setSuccess(true);
    ByocIncomingContractState previousState = state;

    ByocIncomingContractState updatedOracleState =
        serialization.callback(
            context,
            callbackContext,
            state,
            ByocIncomingContract.CallBacks.replaceNonBpOracle(state.getOracleNonce()));

    assertThat(updatedOracleState.getOracleNodes()).isEmpty();
    assertThat(updatedOracleState.getOracleNonce()).isEqualTo(previousState.getOracleNonce() + 1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(s -> LargeOracleRpc.requestNewSmallOracle().write(s));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  /**
   * If small oracle is found to contain only BP oracle members a new small oracle is not requested.
   */
  @Test
  public void replaceNonBpOracleCallbackWithAllBpOracle() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(true);
    callbackContext.setSuccess(true);
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    ByocIncomingContract.CallBacks.replaceNonBpOracle(state.getOracleNonce())))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot replace non-BP oracle as all oracle members of nonce 0 are block producers");
  }

  /** If oracle changed during callback a new small oracle is not requested. */
  @Test
  public void replaceNonBpOracleCallbackOracleNotSame() {
    CallbackContext callbackContext = checkLoStatusCallbackResult(true);
    callbackContext.setSuccess(true);
    long previousNonce = state.getOracleNonce() - 1;
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    ByocIncomingContract.CallBacks.replaceNonBpOracle(previousNonce)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Oracle with nonce " + previousNonce + " has already been changed");
  }

  /**
   * Create large oracle callback result for checking oracle status.
   *
   * @param membersAreLargeOracle true if all small oracle members are part of large oracle
   * @return callback with large oracle result
   */
  private static CallbackContext checkLoStatusCallbackResult(boolean membersAreLargeOracle) {
    byte[] allMembersIsPartOfLargeOracleRpc =
        SafeDataOutputStream.serialize(s -> s.writeBoolean(membersAreLargeOracle));
    SafeDataInputStream callbackCheckResult =
        SafeDataInputStream.createFromBytes(allMembersIsPartOfLargeOracleRpc);
    CallbackContext.ExecutionResult callbackResult =
        CallbackContext.createResult(null, true, callbackCheckResult);
    return CallbackContext.create(FixedList.create(List.of(callbackResult)));
  }

  private void setBlockProductionTime(long blockProductionTime) {
    this.context =
        new SysContractContextTest(blockProductionTime, context.getBlockTime(), context.getFrom());
  }

  @Test
  public void panicsOnDepositOverMax() {
    long depositNonce = state.getDepositNonce();
    newContextWithSender(oracle1);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        this.state,
                        stream -> {
                          stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                          stream.writeLong(depositNonce);
                          receiver.write(stream);
                          maxDeposit.add(Unsigned256.ONE).write(stream);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Deposited amount cannot exceed 250000");
  }

  @Test
  public void createDeposit() {
    assertThat(state.getDeposit()).isNull();
    long depositNonce = state.getDepositNonce();
    assertThat(depositNonce).isEqualTo(0);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            this.state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(depositNonce);
              receiver.write(stream);
              amount.write(stream);
            });

    assertThat(state.getDeposit().hasSigned(0)).isFalse();
    assertThat(state.getDeposit().hasSigned(1)).isTrue();
    assertThat(state.getDeposit().hasSigned(2)).isFalse();
    assertThat(state.getDepositNonce()).isEqualTo(0);
  }

  @Test
  public void createDepositZero() {
    Unsigned256 zero = Unsigned256.ZERO;
    newContextWithSender(oracle1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    this.state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                      stream.writeLong(state.getDepositNonce());
                      receiver.write(stream);
                      zero.write(stream);
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid amount. Amount should be greater than zero");
  }

  @Test
  public void invokeInvalidInvocation() {
    newContextWithSender(receiver);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context, this.state, stream -> stream.writeByte(INVALID_INVOCATION_BYTE)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 100");
  }

  @Test
  public void createDeposit_notOracleNode() {
    newContextWithSender(receiver);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    this.state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                      stream.writeLong(0);
                      receiver.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Transaction did not come from an oracle node");
  }

  @Test
  public void createDeposit_wrongDepositNonce() {
    newContextWithSender(oracle1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    this.state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                      stream.writeLong(1111);
                      receiver.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid deposit nonce");
  }

  @Test
  public void updateExistingDeposit() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            this.state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              amount.write(stream);
            });

    assertThat(state.getDeposit()).isNull();

    deposit = state.getDeposits().getValue(0L);
    assertThat(deposit.hasSigned(0)).isTrue();
    assertThat(deposit.hasSigned(1)).isTrue();
    assertThat(deposit.hasSigned(2)).isFalse();
    assertThat(deposit.hasEnoughSignatures()).isTrue();
    assertThat(state.getDepositNonce()).isEqualTo(1);
  }

  @Test
  public void depositNonceHaveBeenUsed() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);

    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            this.state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              amount.write(stream);
            });
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    this.state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                      stream.writeLong(0);
                      receiver2.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Invalid deposit nonce");
  }

  @Test
  public void updateExistingDeposit_wrongReceiver() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(0);
              oracle2.write(stream);
              amount.write(stream);
            });

    assertThat(getInteractions()).hasSize(1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    assertDisputeCreated(actualRpc);
  }

  @Test
  public void updateExistingDeposit_wrongAmount() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(0);
              receiver.write(stream);
              amount.add(Unsigned256.ONE).write(stream);
            });

    assertThat(getInteractions()).hasSize(1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    assertDisputeCreated(actualRpc);
  }

  private void assertDisputeCreated(byte[] actualRpc) {
    assertThat(state.getDisputes().containsKey(0L)).isTrue();
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.createDispute(oracle1, 0));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void updateExistingDeposit_alreadySigned() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
                      stream.writeLong(0);
                      receiver.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Signature already received from node 0");
  }

  @Test
  public void finalizeExistingDeposit() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, amount);
    deposit = deposit.registerSignature(0);
    deposit = deposit.registerSignature(2);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              amount.write(stream);
            });

    validateCoinIsMinted(receiver, amount, oracles);

    assertThat(state.getDeposit()).isNull();
    assertThat(state.getDepositNonce()).isEqualTo(1);
  }

  @Test
  public void depositSumExceedsMaxAmountTriggersOracleUpdateRequest() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, maxDeposit);
    deposit = deposit.registerSignature(0);
    deposit = deposit.registerSignature(2);
    state = state.setDeposit(deposit);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              maxDeposit.write(stream);
            });
    assertThat(state.getDepositSum()).isEqualTo(maxDeposit);
    assertThat(getInteractions()).hasSize(0);

    Deposit deposit1 = state.getOrCreateDeposit(1, receiver, Unsigned256.ONE);
    deposit1 = deposit1.registerSignature(1);
    deposit1 = deposit1.registerSignature(2);
    state = state.setDeposit(deposit1);

    newContextWithSender(oracle0);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              Unsigned256.ONE.write(stream);
            });

    assertThat(context.getInteractions()).hasSize(1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle());
    assertThat(actualRpc).isEqualTo(expectedRpc);

    assertThat(state.getDepositSum()).isEqualTo(Unsigned256.ZERO);
    assertThat(state.getOracleNodes()).isEmpty();
    Epoch epoch = state.getEpochs().getValue(0L);
    assertThat(epoch).isNotNull();
  }

  @Test
  public void depositSumExceedsMaxAmountStillMintsCoin() {
    Deposit deposit = state.getOrCreateDeposit(0, receiver, maxDeposit);
    state = state.setDeposit(deposit).clearDeposit();
    Deposit deposit1 = state.getOrCreateDeposit(1, receiver, Unsigned256.ONE);
    deposit1 = deposit1.registerSignature(0);
    deposit1 = deposit1.registerSignature(2);

    state = state.setDeposit(deposit1);

    newContextWithSender(oracle1);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              stream.writeLong(state.getDepositNonce());
              receiver.write(stream);
              Unsigned256.ONE.write(stream);
            });

    validateCoinIsMinted(receiver, Unsigned256.ONE, oracles);

    assertThat(state.getDeposit()).isNull();
    assertThat(state.getDepositNonce()).isEqualTo(2);
  }

  private static Unsigned256 getTax(Unsigned256 amount) {
    return amount
        .multiply(Unsigned256.create(ByocIncomingContract.BYOC_TAX_PERMIL))
        .divide(Unsigned256.create(1000));
  }

  private void validateCoinIsMinted(
      BlockchainAddress receiver, Unsigned256 amount, List<BlockchainAddress> oracles) {
    LocalPluginStateUpdate event = context.getUpdateLocalAccountPluginStates().get(0);
    assertThat(event.getContext()).isEqualTo(receiver);
    Unsigned256 tax = getTax(amount);
    assertThat(context.getByocSymbol()).isEqualTo(symbol);
    assertThat(context.getByocFeesGas()).isEqualTo(tax);
    Unsigned256 taxedAmount = amount.subtract(tax);
    assertThat(event.getRpc()).isEqualTo(AccountPluginRpc.addCoinBalance(symbol, taxedAmount));

    byte[] callbackRpc = context.getRemoteCalls().callbackRpc;
    assertThat(callbackRpc)
        .isEqualTo(
            SafeDataOutputStream.serialize(
                ByocIncomingContract.CallBacks.checkMintingEvent(oracles, taxedAmount)));
  }

  @Test
  public void disputeDeposit() {
    long depositNonce = 0L;
    state =
        state.setDeposit(new Deposit(receiver, amount.subtract(Unsigned256.ONE))).clearDeposit();
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    newContextWithSender(oracle0);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_CREATE, tx);

    assertThat(getInteractions()).hasSize(1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.createDispute(oracle0, depositNonce));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    Dispute dispute = state.getDisputes().getValue(depositNonce);
    assertThat(dispute.getChallenger()).isEqualTo(oracle0);
    assertThat(dispute.getClaims()).hasSize(1);
    assertThat(dispute.getVotingResult()).isNull();
    assertThat(dispute.getWinningClaim()).isNull();
    assertThat(dispute.getOriginalClaim()).isEqualTo(tx);
  }

  @Test
  void cannotDisputeDepositWithNoPbcCounterPart() {
    // If a deposit has not been created on PBC it is not fraud, but rather the oracles being
    // inactive.
    long depositNonce = 101L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    newContextWithSender(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_CREATE);
                      writeDisputeTransaction(tx, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No deposit with nonce 101 found");
  }

  @Test
  public void multipleDisputes() {
    state =
        state
            .setDeposit(new Deposit(receiver, amount.subtract(Unsigned256.ONE)))
            .clearDeposit()
            .setDeposit(new Deposit(receiver, Unsigned256.create(100_000)))
            .clearDeposit()
            .setDeposit(new Deposit(receiver3, Unsigned256.create(500_000)))
            .clearDeposit();

    Dispute.Transaction tx1 = new Dispute.Transaction(0L, receiver, amount);
    newContextWithSender(oracle0);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_CREATE, tx1);
    assertThat(getInteractions()).hasSize(1);

    Dispute.Transaction tx1Counter =
        new Dispute.Transaction(0, receiver, amount.add(Unsigned256.ONE));
    state = state.addCounterClaim(tx1Counter).archiveDispute(0L, 1);

    Dispute.Transaction tx2 = new Dispute.Transaction(1L, receiver2, Unsigned256.create(100_000));
    newContextWithSender(oracle2);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_CREATE, tx2);
    assertThat(getInteractions()).hasSize(1);

    state = state.archiveDispute(1L, 0);

    long depositNonce = 2L;
    Dispute.Transaction tx3 =
        new Dispute.Transaction(depositNonce, receiver3, Unsigned256.create(100_000));
    newContextWithSender(oracle1);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_CREATE, tx3);

    assertThat(getInteractions()).hasSize(1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(0).rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.createDispute(oracle1, depositNonce));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    Dispute activeDispute = state.getDisputes().getValue(depositNonce);
    assertThat(activeDispute.getChallenger()).isEqualTo(oracle1);
    assertThat(activeDispute.getClaims()).hasSize(1);
    assertThat(activeDispute.getVotingResult()).isNull();
    assertThat(activeDispute.getWinningClaim()).isNull();
    assertThat(activeDispute.getOriginalClaim()).isEqualTo(tx3);
  }

  @Test
  public void cannotDisputeSameDepositTwice() {
    state = state.setDeposit(new Deposit(receiver, Unsigned256.create(42))).clearDeposit();
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute).archiveDispute(0, 0);

    newContextWithSender(oracle1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_CREATE);
                      writeDisputeTransaction(tx, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputed deposit has already been resolved");
  }

  @Test
  public void cannotDisputeWinningCounterClaimTwice() {
    state = state.setDeposit(new Deposit(receiver, Unsigned256.create(42))).clearDeposit();
    Dispute.Transaction tx =
        new Dispute.Transaction(0, receiver, amount.subtract(Unsigned256.create(15_000)));
    Dispute.Transaction txCounter = new Dispute.Transaction(0, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute).addCounterClaim(txCounter).archiveDispute(0, 1);

    newContextWithSender(oracle1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_CREATE);
                      writeDisputeTransaction(txCounter, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputed deposit has already been resolved");
  }

  @Test
  public void cannotDisputeIfTransactionMatchesDepositInState() {
    state = state.setDeposit(new Deposit(receiver, amount)).clearDeposit();
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_CREATE);
                      writeDisputeTransaction(tx, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputed transaction is equal to the one stored in PBC state");
  }

  @Test
  public void resolveDispute() {
    long depositNonce = 0L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    Dispute.Transaction txCounter =
        new Dispute.Transaction(depositNonce, receiver, amount.subtract(Unsigned256.create(500)));
    Dispute dispute = Dispute.create(oracle0, tx);
    Deposit deposit =
        new Deposit(receiver, amount.add(amount)).registerSignature(2).registerSignature(1);
    state = state.setDeposit(deposit).clearDeposit();
    state = state.addDispute(dispute).addCounterClaim(txCounter);

    state = invokeDisputeResolve(depositNonce, 0);

    List<byte[]> actualRpcList =
        getInteractions().stream()
            .map(interaction -> SafeDataOutputStream.serialize(interaction.rpc))
            .toList();

    validateNewSmallOracleRequestWasSent(actualRpcList.get(0));
    List<BlockchainAddress> cheatingOracles = List.of(oracle1, oracle2);
    validateTypeTwoFraudHandled(
        actualRpcList.get(1), actualRpcList.get(2), amount, cheatingOracles);

    Dispute archivedDispute = state.getDisputes().getValue(depositNonce);
    assertThat(archivedDispute.getVotingResult()).isEqualTo(0);
    assertThat(archivedDispute.getWinningClaim()).isEqualTo(tx);
  }

  @Test
  public void resolveDisputeCurrentDeposit() {
    long depositNonce = 0L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    Deposit deposit =
        new Deposit(receiver, amount.add(amount)).registerSignature(2).registerSignature(1);
    state = state.setDeposit(deposit);
    state = state.addDispute(dispute);

    state = invokeDisputeResolve(depositNonce, 0);

    List<byte[]> actualRpcList =
        getInteractions().stream()
            .map(interaction -> SafeDataOutputStream.serialize(interaction.rpc))
            .toList();

    validateNewSmallOracleRequestWasSent(actualRpcList.get(0));

    List<BlockchainAddress> cheatingOracles = List.of(oracle1, oracle2);
    validatePenalizeCheatingOraclesWasSent(actualRpcList.get(1), cheatingOracles);

    validateCoinIsMinted(receiver, amount, Collections.emptyList());

    // No further actions are taken
    assertThat(actualRpcList.size()).isEqualTo(2);
  }

  @Test
  public void resolveDisputeWithNoFraud() {
    long depositNonce = 101L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute);

    state = invokeDisputeResolve(depositNonce, ByocIncomingContract.DISPUTE_RESULT_FALSE_CLAIM);

    Dispute archivedDispute = state.getDisputes().getValue(depositNonce);
    assertThat(archivedDispute.getVotingResult())
        .isEqualTo(ByocIncomingContract.DISPUTE_RESULT_FALSE_CLAIM);
  }

  @Test
  public void disputeResultInsufficientTokensIsDropped() {
    long depositNonce = 101L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute);

    state =
        invokeDisputeResolve(depositNonce, ByocIncomingContract.DISPUTE_RESULT_INSUFFICIENT_TOKENS);

    Dispute archivedDispute = state.getDisputes().getValue(depositNonce);
    assertThat(archivedDispute).isNull();
  }

  @Test
  public void resolveDisputeWhereNotEnoughCoinWasMinted() {
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    Deposit deposit =
        new Deposit(receiver, Unsigned256.create(500)).registerSignature(0).registerSignature(1);
    state = state.setDeposit(deposit).clearDeposit();
    state = state.addDispute(dispute);

    state = invokeDisputeResolve(0, 0);

    List<BlockchainAddress> cheatingOracles = List.of(oracle0, oracle1);
    byte[] actualRpc = SafeDataOutputStream.serialize(getInteractions().get(1).rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(cheatingOracles));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    validateCoinIsMinted(
        receiver, amount.subtract(Unsigned256.create(500)), Collections.emptyList());
  }

  private List<ContractEventInteraction> getInteractions() {
    return context.getInteractions().stream()
        .map(event -> (ContractEventInteraction) event)
        .toList();
  }

  private List<ContractEventInteraction> getRemoteCalls() {
    return context.getRemoteCalls().contractEvents.stream()
        .map(event -> (ContractEventInteraction) event)
        .toList();
  }

  @Test
  public void createDisputeWithAmountZero() {
    Deposit deposit =
        new Deposit(receiver, Unsigned256.create(500)).registerSignature(0).registerSignature(1);
    state = state.setDeposit(deposit).clearDeposit();
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, Unsigned256.ZERO);
    state = state.setDeposit(deposit);
    assertThatThrownBy(
            () ->
                state =
                    serialization.invoke(
                        context,
                        state,
                        stream -> {
                          stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_CREATE);
                          writeDisputeTransaction(tx, stream);
                        }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot create a dispute for the amount zero");
  }

  @Test
  public void newOracleStartsWithUnsignedDeposit() {
    newContextWithSender(oracle1);
    final Unsigned256 amount = Unsigned256.TEN;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              rpc.writeLong(0);
              oracle0.write(rpc);
              amount.write(rpc);
            });
    Deposit stateDeposit = state.getDeposit();
    assertThat(stateDeposit.amount()).isEqualTo(amount);
    assertThat(stateDeposit.receiver()).isEqualTo(oracle0);
    setBlockProductionTime(state.getOracleTimestamp() + TimeUnit.DAYS.toMillis(28));
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocIncomingContract.Invocations.REQUEST_NEW_ORACLE);
            });
    newContextWithSender(largeOracle);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocIncomingContract.Invocations.UPDATE_ORACLE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(
                  rpc, List.of(receiver, receiver2, receiver3));
              BlockchainPublicKey unusedKey = oracleKey0.getPublic();
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
                  rpc, List.of(unusedKey, unusedKey, unusedKey));
            });
    newContextWithSender(receiver2);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(ByocIncomingContract.Invocations.DEPOSIT);
              rpc.writeLong(0);
              receiver3.write(rpc);
              amount.write(rpc);
            });
    Deposit deposit = state.getDeposit();
    assertThat(deposit.amount()).isEqualTo(amount);
    assertThat(deposit.receiver()).isEqualTo(receiver3);
    assertThat(state.getDisputes().size()).isEqualTo(0);
  }

  @Test
  public void resolveDisputeWhereTooMuchCoinWasMinted() {
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    Deposit deposit =
        new Deposit(receiver, amount.add(Unsigned256.create(10_000)))
            .registerSignature(0)
            .registerSignature(1)
            .registerSignature(2);
    state = state.setDeposit(deposit).clearDeposit();
    state = state.addDispute(dispute);

    invokeDisputeResolve(0, 0);

    List<byte[]> actualRpcList =
        getInteractions().stream()
            .map(interaction -> SafeDataOutputStream.serialize(interaction.rpc))
            .toList();

    validateNewSmallOracleRequestWasSent(actualRpcList.get(0));
    validateTypeTwoFraudHandled(
        actualRpcList.get(1), actualRpcList.get(2), Unsigned256.create(10_000), oracles);
  }

  @Test
  public void resolveDisputeWithWrongReceiver() {
    state = state.endCurrentEpoch().startNewEpoch(oracles, 0);
    Dispute.Transaction ethTx = new Dispute.Transaction(0, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, ethTx);
    Deposit deposit = new Deposit(oracle2, amount).registerSignature(0).registerSignature(2);
    state = state.setDeposit(deposit).clearDeposit();
    state = state.addDispute(dispute);

    invokeDisputeResolve(0, 0);

    List<byte[]> actualRpcList =
        getInteractions().stream()
            .map(interaction -> SafeDataOutputStream.serialize(interaction.rpc))
            .toList();

    validateNewSmallOracleRequestWasSent(actualRpcList.get(0));
    List<BlockchainAddress> cheatingOracles = List.of(oracle0, oracle2);
    validateTypeTwoFraudHandled(
        actualRpcList.get(1), actualRpcList.get(2), amount, cheatingOracles);
    validateCoinIsMinted(receiver, amount, Collections.emptyList());
  }

  @Test
  public void resolveDisputeFromPreviousEpoch() {
    BlockchainAddress oracle3 =
        BlockchainAddress.fromString("000000000000000000000000000000000000004242");
    List<BlockchainAddress> newOracles = List.of(this.oracle2, oracle3, oracle0);
    Deposit deposit = new Deposit(receiver, amount).registerSignature(1).registerSignature(0);
    state = state.setDeposit(deposit).clearDeposit().endCurrentEpoch().startNewEpoch(newOracles, 0);

    Dispute.Transaction tx =
        new Dispute.Transaction(0, receiver, amount.subtract(Unsigned256.create(150L)));
    Dispute dispute = Dispute.create(oracle3, tx);
    state = state.addDispute(dispute);

    invokeDisputeResolve(0, 0);

    List<byte[]> actualRpcList =
        getInteractions().stream()
            .map(interaction -> SafeDataOutputStream.serialize(interaction.rpc))
            .toList();
    List<BlockchainAddress> cheatingOracles = List.of(this.oracle0, oracle1);
    validateTypeTwoFraudHandled(
        actualRpcList.get(0), actualRpcList.get(1), Unsigned256.create(150L), cheatingOracles);
  }

  private void validateNewSmallOracleRequestWasSent(byte[] actualRpc) {
    byte[] expectedRpc = SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle());
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private void validateTypeTwoFraudHandled(
      byte[] actualBurnTokensRpc,
      byte[] actualRecalibrateTwinsRpc,
      Unsigned256 amount,
      List<BlockchainAddress> cheatingOracles) {
    byte[] expectedBurnTokensRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(cheatingOracles));
    assertThat(actualBurnTokensRpc).isEqualTo(expectedBurnTokensRpc);

    // Handle type 2 fraud
    byte[] expectedRecalibrateTwinsRpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.recalibrateTokens(state.getSymbol(), amount, cheatingOracles));
    assertThat(actualRecalibrateTwinsRpc).isEqualTo(expectedRecalibrateTwinsRpc);
  }

  private void validatePenalizeCheatingOraclesWasSent(
      byte[] actualBurnTokensRpc, List<BlockchainAddress> cheatingOracles) {
    byte[] expectedBurnTokensRpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(cheatingOracles));
    assertThat(actualBurnTokensRpc).isEqualTo(expectedBurnTokensRpc);
  }

  private ByocIncomingContractState invokeDisputeResolve(long depositNonce, int vote) {
    newContextWithSender(largeOracle);
    return serialization.invoke(
        context,
        state,
        stream -> {
          stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_RESULT);
          stream.writeLong(0);
          stream.writeLong(depositNonce);
          stream.writeInt(vote);
        });
  }

  @Test
  public void dropDispute() {
    Dispute.Transaction tx = new Dispute.Transaction(101, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute);

    state = invokeDisputeResolve(101, -1);

    assertThat(state.getDisputes().getValue(0L)).isNull();
  }

  @Test
  public void onlyLargeOracleContractCanResolveDisputes() {
    newContextWithSender(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_RESULT);
                      stream.writeLong(0);
                      stream.writeLong(0);
                      stream.writeInt(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute can only be resolved by the large oracle contract");
  }

  @Test
  public void canOnlyResolveDisputesIfItExists() {
    newContextWithSender(largeOracle);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_RESULT);
                      stream.writeLong(0L);
                      stream.writeLong(0L);
                      stream.writeInt(10);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute does not exists or has already been resolved");
  }

  @Test
  public void canOnlyResolveDisputesIfItIsNotAlreadyResolved() {
    state =
        state
            .addDispute(Dispute.create(oracle0, new Dispute.Transaction(0, receiver, amount)))
            .archiveDispute(0, 0);
    newContextWithSender(largeOracle);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_RESULT);
                      stream.writeLong(0L);
                      stream.writeLong(0L);
                      stream.writeInt(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Dispute does not exists or has already been resolved");
  }

  @Test
  public void addCounterClaim() {
    long depositNonce = 101L;
    Dispute.Transaction tx = new Dispute.Transaction(depositNonce, receiver, amount);
    Dispute dispute = Dispute.create(oracle0, tx);
    state = state.addDispute(dispute);

    newContextWithSender(largeOracle);
    Dispute.Transaction txCounter = new Dispute.Transaction(depositNonce, oracle0, amount);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM, txCounter);
    assertThat(state.getDisputes().getValue(depositNonce).getClaims()).hasSize(2);

    txCounter = new Dispute.Transaction(depositNonce, receiver, amount.add(amount));
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM, txCounter);
    assertThat(state.getDisputes().getValue(depositNonce).getClaims()).hasSize(3);

    // Assert that counter claims that has already been added, cannot be added again
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM, tx);
    invokeClaim(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM, txCounter);
    assertThat(state.getDisputes().getValue(depositNonce).getClaims()).hasSize(3);
  }

  @Test
  public void onlyLargeOracleContractCanAddCounterClaim() {
    newContextWithSender(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM);
                      stream.writeLong(0);
                      receiver.write(stream);
                      amount.write(stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Counter-claims can only be added through the large oracle contract");
  }

  @Test
  public void canOnlyAddCounterClaimIfDisputeExists() {
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    newContextWithSender(largeOracle);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM);
                      writeDisputeTransaction(tx, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No dispute to add counter-claim to");
  }

  @Test
  public void counterClaimOnFinishedDispute() {
    Dispute.Transaction tx = new Dispute.Transaction(0, receiver, amount);
    state = state.addDispute(Dispute.create(oracle0, tx)).archiveDispute(0, 0);
    newContextWithSender(largeOracle);

    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM);
                      writeDisputeTransaction(tx, stream);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot add counter-claim to resolved dispute");
  }

  @Test
  public void testFraudTypes() {
    Deposit deposit = new Deposit(receiver, amount);
    Dispute.Transaction transaction = new Dispute.Transaction(0, receiver, amount);

    assertThat(ByocIncomingContract.isType1Fraud(deposit, transaction)).isFalse();
    assertThat(ByocIncomingContract.isType2Fraud(deposit, transaction)).isFalse();

    assertThat(ByocIncomingContract.isType1Fraud(null, transaction)).isTrue();
    assertThat(ByocIncomingContract.isType2Fraud(null, transaction)).isFalse();

    deposit = new Deposit(receiver2, amount);
    assertThat(ByocIncomingContract.isType1Fraud(deposit, transaction)).isTrue();
    assertThat(ByocIncomingContract.isType2Fraud(deposit, transaction)).isTrue();

    deposit = new Deposit(receiver, amount.subtract(Unsigned256.ONE));
    assertThat(ByocIncomingContract.isType1Fraud(deposit, transaction)).isTrue();
    assertThat(ByocIncomingContract.isType2Fraud(deposit, transaction)).isFalse();

    deposit = new Deposit(receiver, amount.add(Unsigned256.ONE));
    assertThat(ByocIncomingContract.isType1Fraud(deposit, transaction)).isFalse();
    assertThat(ByocIncomingContract.isType2Fraud(deposit, transaction)).isTrue();
  }

  @Test
  public void updateSmallOracle() {
    KeyPair oracle3Keys = new KeyPair();
    BlockchainAddress oracle3 = oracle3Keys.getPublic().createAddress();
    List<BlockchainAddress> newOracles = List.of(oracle3, oracle2, oracle0);
    List<BlockchainPublicKey> oracleSigKeys =
        List.of(oracle3Keys.getPublic(), oracleKey0.getPublic(), oracleKey1.getPublic());

    newContextWithSender(largeOracle);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.UPDATE_ORACLE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, newOracles);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, oracleSigKeys);
            });

    assertThat(state.getOracleNodes()).isEqualTo(newOracles);
  }

  @Test
  public void updateSmallOracleWithPreviousEpoch() {
    Deposit deposit = new Deposit(receiver, amount);
    state =
        state
            .setDeposit(deposit)
            .clearDeposit()
            .setDeposit(deposit)
            .clearDeposit()
            .endCurrentEpoch()
            .startNewEpoch(oracles, 0)
            .setDeposit(deposit)
            .clearDeposit()
            .endCurrentEpoch();

    BlockchainAddress oracle3 =
        BlockchainAddress.fromString("000000000000000000000000000000000000004444");
    List<BlockchainAddress> oracleIdentities = List.of(oracle0, oracle3, oracle1);
    KeyPair oracle3Keys = new KeyPair();
    List<BlockchainPublicKey> oraclePubKeys =
        List.of(oracleKey0.getPublic(), oracle3Keys.getPublic(), oracleKey1.getPublic());

    newContextWithSender(largeOracle);
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(ByocIncomingContract.Invocations.UPDATE_ORACLE);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, oracleIdentities);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, oraclePubKeys);
            });

    assertThat(state.getOracleNonce()).isEqualTo(2L);
    assertThat(state.getEpochs()).extracting(AvlTree::size).isEqualTo(2);
    assertThat(state.getOracleNodes()).isEqualTo(oracleIdentities);

    Epoch lastEpoch = state.getEpochs().getValue(1L);
    assertThat(lastEpoch.getFromDepositNonce()).isEqualTo(2L);
    assertThat(lastEpoch.getToDepositNonce()).isEqualTo(3L);
    assertThat(lastEpoch.getOracles()).isEqualTo(oracles);
  }

  @Test
  public void onlyLargeOracleContractCanUpdateOracle() {
    newContextWithSender(oracle0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    stream -> {
                      stream.writeByte(ByocIncomingContract.Invocations.UPDATE_ORACLE);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, List.of());
                      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, List.of());
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Updates to the small oracle can only be done through the large oracle contract");
  }

  @Test
  public void unknownInvocation() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create(List.of()));
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    state,
                    stream -> {
                      stream.writeByte(10);
                      // Does not matter is not read
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid shortname: 10");
  }

  @Test
  public void newCallback() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create());
    callbackContext.setSuccess(false);
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            rpc -> {
              rpc.writeByte(1);
              amount.write(rpc);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracles);
            });

    assertThat(state).isNotNull();

    assertThat(context.getByocSymbol()).isEqualTo(symbol);
    assertThat(context.getByocFeesGas()).isEqualTo(amount);
    assertThat(context.getByocNodes()).isEqualTo(oracles);
  }

  @Test
  void mintEventCallbackDoesNothingOnSuccess() {
    CallbackContext callbackContext = CallbackContext.create(FixedList.create());
    callbackContext.setSuccess(true);
    state =
        serialization.callback(
            context,
            callbackContext,
            state,
            ByocIncomingContract.CallBacks.checkMintingEvent(oracles, amount));

    assertThat(state).isNotNull();

    assertThat(context.getByocSymbol()).isNull();
    assertThat(context.getByocFeesGas()).isNull();
    assertThat(context.getByocNodes()).isEmpty();
  }

  @Test
  void onUpgradeFromSimpleState() {
    ByocIncomingContract contract = new ByocIncomingContract();
    StateAccessor stateAccessor = StateAccessor.create(state);

    ByocIncomingContractState newState = contract.upgrade(stateAccessor);

    assertThat(newState.getLargeOracle()).isEqualTo(largeOracle);
    assertThat(newState.getSymbol()).isEqualTo(symbol);
    assertThat(newState.getOracleNodes()).isEqualTo(oracles);
    assertThat(newState.getMaximumDepositPerEpoch()).isEqualTo(maxDeposit);
  }

  @Test
  void onUpgradeFromComplexState() {
    ByocIncomingContractState complexState = createComplexState(state);

    ByocIncomingContract contract = new ByocIncomingContract();
    StateAccessor stateAccessor = StateAccessor.create(complexState);

    ByocIncomingContractState migratedState = contract.upgrade(stateAccessor);

    assertThat(migratedState.getOracleNodes()).isEqualTo(complexState.getOracleNodes());

    assertThat(migratedState.getDepositNonce()).isEqualTo(complexState.getDepositNonce());
    assertDepositEquals(migratedState.getDeposit(), complexState.getDeposit());
    assertDepositsAreEqual(migratedState, complexState);

    assertThat(migratedState.getDepositSum()).isEqualTo(complexState.getDepositSum());

    assertDisputesAreEqual(migratedState, complexState);
    assertThat(migratedState.getOracleNonce()).isEqualTo(complexState.getOracleNonce());

    assertEpochsAreEqual(migratedState, complexState);
    assertThat(migratedState.getOracleTimestamp()).isEqualTo(complexState.getOracleTimestamp());
  }

  private void assertDepositsAreEqual(
      ByocIncomingContractState newState, ByocIncomingContractState oldState) {
    AvlTree<Long, Deposit> migratedDeposits = newState.getDeposits();
    AvlTree<Long, Deposit> oldDeposits = oldState.getDeposits();
    assertThat(migratedDeposits.size()).isEqualTo(oldDeposits.size());
    for (Long nonce : oldDeposits.keySet()) {
      Deposit migratedDeposit = migratedDeposits.getValue(nonce);
      Deposit oldDeposit = oldDeposits.getValue(nonce);
      assertDepositEquals(migratedDeposit, oldDeposit);
    }
  }

  private void assertDepositEquals(Deposit migratedDeposit, Deposit oldDeposit) {
    if (oldDeposit == null) {
      assertThat(migratedDeposit).isNull();
    } else {
      assertThat(migratedDeposit.amount()).isEqualTo(oldDeposit.amount());
      assertThat(migratedDeposit.receiver()).isEqualTo(oldDeposit.receiver());
      assertThat(migratedDeposit.hasSigned(0)).isEqualTo(oldDeposit.hasSigned(0));
      assertThat(migratedDeposit.hasSigned(1)).isEqualTo(oldDeposit.hasSigned(1));
      assertThat(migratedDeposit.hasSigned(2)).isEqualTo(oldDeposit.hasSigned(2));
    }
  }

  private void assertDisputesAreEqual(
      ByocIncomingContractState newState, ByocIncomingContractState oldState) {
    AvlTree<Long, Dispute> migratedDisputes = newState.getDisputes();
    AvlTree<Long, Dispute> oldDisputes = oldState.getDisputes();
    assertThat(migratedDisputes.size()).isEqualTo(oldDisputes.size());
    for (Long nonce : oldDisputes.keySet()) {
      Dispute migratedDispute = migratedDisputes.getValue(nonce);
      Dispute oldDispute = oldDisputes.getValue(nonce);
      assertThat(migratedDispute.getChallenger()).isEqualTo(oldDispute.getChallenger());
      assertThat(migratedDispute.getClaims()).isEqualTo(oldDispute.getClaims());
      assertThat(migratedDispute.getVotingResult()).isEqualTo(oldDispute.getVotingResult());
    }
  }

  private void assertEpochsAreEqual(
      ByocIncomingContractState newState, ByocIncomingContractState oldState) {
    AvlTree<Long, Epoch> migratedEpochs = newState.getEpochs();
    AvlTree<Long, Epoch> oldEpochs = oldState.getEpochs();
    assertThat(migratedEpochs.size()).isEqualTo(oldEpochs.size());
    for (Long nonce : oldEpochs.keySet()) {
      Epoch migratedEpoch = migratedEpochs.getValue(nonce);
      Epoch oldEpoch = oldEpochs.getValue(nonce);
      assertThat(migratedEpoch.getToDepositNonce()).isEqualTo(oldEpoch.getToDepositNonce());
      assertThat(migratedEpoch.getFromDepositNonce()).isEqualTo(oldEpoch.getFromDepositNonce());
      assertThat(migratedEpoch.getOracles()).isEqualTo(oldEpoch.getOracles());
    }
  }

  private ByocIncomingContractState createComplexState(ByocIncomingContractState state) {
    ByocIncomingContractState updatedState = state;

    Deposit deposit = new Deposit(receiver, amount).registerSignature(0).registerSignature(1);
    updatedState = updatedState.setDeposit(deposit).clearDeposit();
    deposit =
        new Deposit(receiver3, amount.add(Unsigned256.ONE))
            .registerSignature(2)
            .registerSignature(0);
    updatedState = updatedState.setDeposit(deposit).clearDeposit();
    updatedState = updatedState.endCurrentEpoch();
    Dispute.Transaction tx = new Dispute.Transaction(1, receiver3, amount);
    Dispute dispute = Dispute.create(receiver, tx);
    updatedState = updatedState.addDispute(dispute).archiveDispute(1, 0);
    BlockchainAddress oracle3 =
        BlockchainAddress.fromString("000000000000000000000000000000000000004444");
    List<BlockchainAddress> newOracle = List.of(oracle0, oracle3, oracle1);
    updatedState = updatedState.startNewEpoch(newOracle, 96);

    deposit = new Deposit(receiver2, amount).registerSignature(1);
    updatedState = updatedState.setDeposit(deposit).clearDeposit();

    deposit = new Deposit(receiver2, maxDeposit).registerSignature(2);
    updatedState = updatedState.setDeposit(deposit);

    tx = new Dispute.Transaction(2, receiver2, amount);
    dispute = Dispute.create(receiver, tx);
    updatedState = updatedState.addDispute(dispute);

    return updatedState;
  }

  private void invokeClaim(int invocation, Dispute.Transaction transaction) {
    state =
        serialization.invoke(
            context,
            state,
            stream -> {
              stream.writeByte(invocation);
              writeDisputeTransaction(transaction, stream);
            });
  }

  private static void writeDisputeTransaction(
      Dispute.Transaction transaction, SafeDataOutputStream stream) {
    stream.writeLong(transaction.nonce());
    transaction.receiver().write(stream);
    transaction.amount().write(stream);
  }

  private void newContextWithSender(BlockchainAddress oracle) {
    context =
        new SysContractContextTest(
            context.getBlockProductionTime(), context.getBlockTime(), oracle);
  }
}
