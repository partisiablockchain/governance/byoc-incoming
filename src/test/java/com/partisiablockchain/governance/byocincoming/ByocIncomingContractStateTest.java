package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ByocIncomingContractStateTest {

  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private static final String symbol = "TEST_SYMBOL";
  private final List<BlockchainAddress> originalOracles =
      List.of(
          BlockchainAddress.fromString("000000000000000000000000000000000000001111"),
          BlockchainAddress.fromString("000000000000000000000000000000000000002222"),
          BlockchainAddress.fromString("000000000000000000000000000000000000003333"));

  private final BlockchainAddress receiver =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  private ByocIncomingContractState state =
      new ByocIncomingContractState(
          largeOracleContract, symbol, originalOracles, Unsigned256.create(50_000L));

  @Test
  public void getOracleNonceFromWithdrawNonce() {
    assertThat(state.getOracleNonceForDepositNonce(0L)).isEqualTo(0L);
    List<BlockchainAddress> oracles2 =
        List.of(
            BlockchainAddress.fromString("000000000000000000000000000000000000004444"),
            BlockchainAddress.fromString("000000000000000000000000000000000000005555"),
            BlockchainAddress.fromString("000000000000000000000000000000000000006666"));
    List<BlockchainAddress> oracles3 =
        List.of(
            BlockchainAddress.fromString("000000000000000000000000000000000000007777"),
            BlockchainAddress.fromString("000000000000000000000000000000000000008888"),
            BlockchainAddress.fromString("000000000000000000000000000000000000009999"));

    state =
        state
            .setDeposit(new Deposit(receiver, Unsigned256.ONE))
            .clearDeposit()
            .setDeposit(new Deposit(receiver, Unsigned256.create(2)))
            .clearDeposit()
            .endCurrentEpoch()
            .startNewEpoch(oracles2, 0)
            .setDeposit(new Deposit(receiver, Unsigned256.create(3L)))
            .clearDeposit()
            .setDeposit(new Deposit(receiver, Unsigned256.create(4L)))
            .clearDeposit()
            .setDeposit(new Deposit(receiver, Unsigned256.create(5L)))
            .clearDeposit()
            .setDeposit(new Deposit(receiver, Unsigned256.create(6L)))
            .clearDeposit()
            .endCurrentEpoch()
            .startNewEpoch(oracles3, 0)
            .setDeposit(new Deposit(receiver, Unsigned256.create(7L)))
            .clearDeposit()
            .endCurrentEpoch()
            .startNewEpoch(originalOracles, 0);

    assertThat(state.getOracleNonceForDepositNonce(-1)).isEqualTo(-1);
    assertThat(state.getOracleNonceForDepositNonce(0)).isEqualTo(0);
    assertThat(state.getOracleNonceForDepositNonce(1)).isEqualTo(0);
    assertThat(state.getOracleNonceForDepositNonce(2)).isEqualTo(1);
    assertThat(state.getOracleNonceForDepositNonce(5)).isEqualTo(1);
    assertThat(state.getOracleNonceForDepositNonce(6)).isEqualTo(2);
    assertThat(state.getOracleNonceForDepositNonce(7)).isEqualTo(3);
  }
}
