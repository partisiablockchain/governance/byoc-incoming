package com.partisiablockchain.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.junit.jupiter.api.Test;

final class RpcTest {

  @Test
  void accountPlugin_addCoinBalance() {
    String symbol = "ETH";
    Unsigned256 amount = Unsigned256.TEN;

    byte[] rpc = AccountPluginRpc.addCoinBalance(symbol, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ADD_COIN_BALANCE);
    assertThat(stream.readString()).isEqualTo(symbol);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount);
  }

  @Test
  void largeOracle_createDispute() {
    BlockchainAddress challenger =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    int transactionNonce = 2;

    byte[] rpc =
        SafeDataOutputStream.serialize(LargeOracleRpc.createDispute(challenger, transactionNonce));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.DISPUTE_CREATE);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(challenger);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.DISPUTE_TOKEN_COST);
    assertThat(stream.readLong()).isEqualTo(0);
    assertThat(stream.readLong()).isEqualTo(transactionNonce);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(ByocIncomingContract.Invocations.DISPUTE_RESULT);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(ByocIncomingContract.Invocations.DISPUTE_COUNTER_CLAIM);
  }

  @Test
  void largeOracle_burnTokens() {
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress account2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");

    List<BlockchainAddress> cheatingOracles = List.of(account, account2);
    byte[] rpc = SafeDataOutputStream.serialize(LargeOracleRpc.burnTokens(cheatingOracles));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.BURN_TOKENS);
    assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream)).isEqualTo(cheatingOracles);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.DISPUTE_TOKEN_COST);
  }

  @Test
  void largeOracle_recalibrateTokens() {
    String symbol = "ETH";
    Unsigned256 amount = Unsigned256.TEN;
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress account2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");

    List<BlockchainAddress> cheatingOracles = List.of(account, account2);
    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleRpc.recalibrateTokens(symbol, amount, cheatingOracles));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.RECALIBRATE_TOKENS);
    assertThat(stream.readString()).isEqualTo(symbol);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount);
    assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream)).isEqualTo(cheatingOracles);
  }

  @Test
  void largeOracle_requestNewSmallOracle() {
    byte[] rpc = SafeDataOutputStream.serialize(LargeOracleRpc.requestNewSmallOracle());
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(LargeOracleRpc.REQUEST_NEW_SMALL_ORACLE);
    assertThat(stream.readLong()).isEqualTo(LargeOracleRpc.REQUIRED_ORACLE_STAKE);
    assertThat(stream.readUnsignedByte()).isEqualTo(ByocIncomingContract.Invocations.UPDATE_ORACLE);
    assertThat(stream.readDynamicBytes()).isEqualTo(new byte[0]);
  }

  @Test
  void withdrawCallback() {
    BlockchainAddress account1 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress account2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    List<BlockchainAddress> accounts = List.of(account1, account2);
    Unsigned256 amount = Unsigned256.TEN;

    byte[] rpc =
        SafeDataOutputStream.serialize(
            ByocIncomingContract.CallBacks.checkMintingEvent(accounts, amount));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(ByocIncomingContract.CallBacks.CHECK_MINTING_EVENT);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount);
    assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream)).isEqualTo(accounts);
  }
}
